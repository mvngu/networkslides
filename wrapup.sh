#!/usr/bin/env bash

###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Wrap up a source distribution.

ROOTDIR=$(pwd)
cd "$ROOTDIR"
NAME="netslides"
VERSION=$1  # provide version number from command line

# Print usage information for this script.
usage() {
    echo "Usage: $0 ver"
    echo "ver   The version number of the repository."
}

# You must provide an argument to this script.
if [ "$#" -eq 0 ]; then
    usage
    exit 1
fi

build() {
    # Compile the talk slides first, before actually doing the wrapping up.
    # At this stage, it is assumed that the whole source tree compiles OK
    # without any errors.  It is up to you to ensure this.  This function
    # should invoke the file ROOTDIR/Makefile.
    make 2>&1 > /dev/null
}

build
# Test if there is a file called "talk.pdf".  That file results from
# running the command "make" from ROOTDIR.
if [ -e "talk.pdf" ] && [ -f "talk.pdf" ]; then
    cd ..
    cp -rf "$ROOTDIR" "$NAME-$VERSION"
    mv "$NAME-$VERSION"/talk.pdf "$NAME-$VERSION".pdf
    rm -rf "$NAME-$VERSION"/.git
    rm -rf "$NAME-$VERSION"/.gitignore
    rm "$NAME-$VERSION"/talk.bbl
    tar -jcf "$NAME-$VERSION".tar.bz2 "$NAME-$VERSION"
    rm -rf "$NAME-$VERSION"
    exit 0
else
    echo "File talk.pdf does not exist. Exiting..."
    exit 1
fi
