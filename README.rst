This contains some standard stuff on network science.  The whole thing
can be used as a template for typesetting your presentation notes.

Ensure you have a proper and working TeX/LaTeX installation.  To
compile the talk slides, issue the command::

    $ make

Everything is licensed under the terms of the GNU Free Documentation
License version 1.3.  For the full terms of the license, see

http://www.gnu.org/licenses/fdl.html
