%% Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
%% This work is licensed under a the GNU Free Documentation License version
%% 1.3.  For the full terms of the license, see
%% http://www.gnu.org/licenses/fdl.html

\documentclass{beamer}

\usepackage{mystyle}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title[Complex networks]{
  Complex networks
}
\institute{
  \large University of Melbourne
}
\author[Minh Van Nguyen]{
  Minh Van Nguyen \\
  \url{mvngu.name@gmail.com}
}
%% uncomment the following line for the current date
\date{\small \today}
%% \date{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\frame{\titlepage}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\frame{
  \frametitle{Contents}
  \tableofcontents
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{What is a network?}
%%
\frame{
  \frametitle{What is a network?}
  %%
  \begin{itemize}
  \item A structure consisting of:
    \begin{itemize}
    \item \textbf{nodes}

    \item \textbf{links} between nodes

    \item \textbf{data} about the nodes and links
    \end{itemize}

  \item Underlying representation: a combinatorial graph.

  \item A \emph{graph} $G = (V, E)$ is an ordered pair of finite
    sets.  Elements of $V$ are called \emph{vertices} or
    \emph{nodes}.  Elements of $E \subseteq V \times V$ are called
    \emph{edges} or \emph{links}.
  \end{itemize}
}
%%
\frame{
  \frametitle{Examples of networks}
  %%
  \begin{figure}[!htbp]
  \centering
  %% \subfigure[Synthetic network.]{
  %%   \includegraphics[scale=0.6]{image/weighted-multigraph}
  %% }
  %%
  \subfigure[Zachary karate club.]{
    \label{fig:Zachary_karate_club}
    \includegraphics[scale=0.5]{image/Zachary-karate-club_graph}
  }
  %%
  \;
  %%
  \subfigure[Bernoulli family tree.]{
    \label{fig:Bernoulli_family_tree}
    \includegraphics[scale=0.5]{image/Bernoulli-family-tree}
  }
  %%
  \subfigure[Naphthalene.]{
    \label{fig:naphthalene}
    \includegraphics[scale=0.6]{image/molecular-graphs_naphthalene}
  }
  %%
  \;\;
  %%
  \subfigure[Linux filesystem hierarchy.]{
    \label{fig:Linux_filesystem_hierarchy}
    \includegraphics[scale=0.5]{image/filesystem-hierarchy}
  }
  \caption{Examples of (a)~social, (b)~genealogical, (c)~scientific,
    and (d)~technological networks.}
  \end{figure}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{What do we measure?}
%%
\frame{
  \frametitle{What do we measure?}
  %%
  \begin{itemize}
  \item \textbf{Degree distribution}: a histogram of the number of
    links in a network $G = (V, E)$.

  \item If $k \geq 0$ is an integer, then $p_k$ is the proportion of
    nodes in $G$, where each node has $k$ as its number of links.
  \end{itemize}
  %%
  \begin{figure}[!htbp]
  \centering
  \subfigure[Power grid network~\cite{WattsStrogatz1998}.]{
    \includegraphics[scale=0.8]{image/degree-distribution-power-grid_log}
  }
  %%
  \subfigure[C. elegans neural
    network~\cite{WattsStrogatz1998,WhiteEtAl1986}.]{
    \includegraphics[scale=0.8]{image/degree-distribution-C-elegans_log}
  }
  \caption{Degree distributions of (a)~a power grid network, and
    (b)~the neural network of a worm.}
  \end{figure}
}
%%
\frame{
  \frametitle{What do we measure?}
  %%
  \begin{itemize}
  \item \textbf{Distance}: the smallest number of links separating two
    distinct nodes.  \emph{Average distance}: the average distance
    in a network.

  \item \textbf{Clustering coefficient}: the ``cliquishness'' of nodes
    in a network.  For example, choose any two of your friends.  How
    likely are they to be friends of each other?
    %% The clustering coefficient is defined as
    %% $C(G) = \frac{T_c}{T_c + T_o}$, where $T_c$ and $T_o$ count the
    %% number of closed and open triplets, respectively.
    %%
    %% $T_c$ counts the number of closed triplets := 3 nodes each of
    %% which is linked to the other two
    %% $T_o$ counts the number of open triplets := 3 nodes with
    %% exactly one node linking to the other two

  \item \textbf{Node centrality}:  Which node(s) have the greatest
    number of links?  Who have the greatest number of friends?  Which
    websites are the most popular on the internet?  Who or what are
    the gatekeepers?

  \item \textbf{Mixing ratio}: how likely is it that nodes with large
    number of links connect with nodes having small number of links?
    %% The mixing ratio, or degree assortativity, of $G = (V, E)$ is
    %% defined in terms of the Pearson correlation coefficient as
    %% follows:
    %% %%
    %% \begin{align*}
    %% r
    %% =
    %% \frac
    %% {\frac{1}{m} \sum_{e \in E} j_e k_e
    %%  - \big( \frac{1}{2} \sum_{e \in E} (j_e + k_e) \big)^2}
    %% {\frac{1}{2} \sum_{e \in E} (j_e^2 + k_e^2)
    %%  - \big( \frac{1}{2} \sum_{e \in E} (j_e + k_e) \big)^2}
    %% \end{align*}
    %% %%
    %% where $j_e$ and $k_e$ are the degrees of the two endpoints of edge
    %% $e$, and $-1 \leq r \leq 1$.
  \end{itemize}
  %% %%
  %% \begin{figure}[!htbp]
  %% \centering
  %% \subfigure[Zachary karate club network~\cite{Zachary1977}.] {
  %%   \includegraphics[scale=0.49]{image/distance-distribution_karate-club}
  %% }
  %% \subfigure[C. elegans neural
  %%   network~\cite{WattsStrogatz1998,WhiteEtAl1986}.] {
  %%   \includegraphics[scale=0.49]{image/distance-distribution_C-elegans}
  %% }
  %% \subfigure[Power grid network~\cite{WattsStrogatz1998}.] {
  %%   \includegraphics[scale=0.62]{image/distance-distribution_power-grid}
  %% }
  %% %%
  %% \qquad
  %% %%
  %% \subfigure[Co-authorship network~\cite{Newman2001b}.] {
  %%   \includegraphics[scale=0.62]{image/distance-distribution_condensed-matter}
  %% }
  %% \caption{Distance distributions for various real-world networks. The
  %%   horizontal axis represents distance and the vertical axis represents
  %%   the probability that a uniformly chosen pair of distinct vertices
  %%   from the network has the corresponding distance between them.}
  %% \end{figure}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Types of networks}
%%
\frame{
  \frametitle{Types of networks}
  %%
  \begin{itemize}
  \item \textbf{regular}.
    %% Each node in the network has the same number of links $m$.
    Degree distribution is $p_k = 1$ if $k = m$ and $p_k = 0$
    otherwise.  High clustering coefficient; high average distance.

  \item \textbf{Erd\H{o}s-R\'enyi}~\cite{ErdosRenyi1959,ErdosRenyi1960}.
    %% A random network where each link has been chosen uniformly at
    %% random.
    Binomial degree distribution, but the distribution is
    approximately Poisson as the number of nodes $n \to \infty$.  Low
    clustering coefficient; low average distance.
  \end{itemize}
  %%
  \begin{figure}[!htbp]
  \centering
  \subfigure[Regular.]{
    \includegraphics[scale=0.5]{image/k-circulant-graphs_k4}
  }
  %%
  \quad
  %%
  \subfigure[Erd\H{o}s-R\'enyi.]{
    \includegraphics[scale=0.38]{image/erdos-renyi-graph}
  }
  \caption{(a)~A regular network where each node has degree $m = 4$.
    (b)~An Erd\H{o}s-R\'enyi network on $20$ nodes; each link was
    sampled with probability $p = 0.1$.}
  \end{figure}
}
%%
\frame{
  \frametitle{Types of networks}
  %%
  \begin{itemize}
  \item \textbf{small-world}~\cite{WattsStrogatz1998}.
    %% A network that lies somewhere between a regular network and an
    %% Erd\H{o}s-R\'enyi network.  Start with a regular network where
    %% each node has an even degree $m$.  Rewire a proportion $p$ of
    %% the edges.
    Degree distribution is commonly a power-law~\cite{BarratWeigt2000}.
    %% or a power-law with an exponential cut-off.
    Clustering coefficient is approximately
    $C(G) \approx \frac{3(m/2 - 1)}{2(m - 1)} (1 - p)^3$.  As
    $p \to 1$, the average distance approaches
    $\frac{\log n}{\log m}$.
    %% n := number of nodes
    %% m := the even degree in the original regular network from which
    %%      the small-world network is derived via edge rewiring
    That is, high clustering coefficient and low average distance.
    ``Six degree of separation.''
  \end{itemize}
  %%
  \begin{figure}[!htbp]
  \centering
  \subfigure[$p = 0$, regular]{
    \includegraphics[scale=0.6]{image/k-circulant-small-world-random_a}
  }
  \quad
  \subfigure[$p = 0.3$, SW]{
    \includegraphics[scale=0.6]{image/k-circulant-small-world-random_b}
  }
  \quad
  \subfigure[$p = 1$, random]{
    \includegraphics[scale=0.6]{image/k-circulant-small-world-random_c}
  }
  \caption{With increasing randomness, regular networks evolve to
    exhibit properties of Erd\H{o}s-R\'enyi random  networks.
    Small-world networks are intermediate between regular and
    Erd\H{o}s-R\'enyi networks.}
  \end{figure}
}
%%
\frame{
  \frametitle{Types of networks}
  %%
  \begin{itemize}
  \item \textbf{scale-free}~\cite{BarabasiAlbert1999}.
    %% The term ``scale-free'' refers to the scale-free nature of the
    %% degree distribution.
    Commonly small-world.  Constructed based on two principles: growth
    and preferential attachment.
    %% Over time, the network should grow by adding new nodes and
    %% links.  A new node is more likely to be linked to an existing
    %% node with a high degree.
    Degree distribution is a power-law of the form
    $p_k \sim ck^{-\gamma}$, where $c$ is a constant and
    $2 < \gamma < 3$.  The ``rich get richer'' effect.
  \end{itemize}
  %%
  \begin{figure}[!htbp]
  \centering
  \subfigure[US patent citation.]{
    \includegraphics[scale=0.6]{image/US-patent-citation-network}
  }
  %%
  \qquad
  %%
  \subfigure[Actor collaboration.]{
    \includegraphics[scale=0.6]{image/actor-collaboration-network}
  }
  %% \subfigure[Google web graph.]{
  %%   \includegraphics[scale=0.4]{image/Google-web-graph}
  %% }
  %% \subfigure[LiveJournal friendship.]{
  %%   \includegraphics[scale=0.4]{image/livejournal-friendship-network}
  %% }
  \caption{Information and professional collaboration networks exhibit
    power-law degree distributions.}
  \end{figure}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Community structure}
%%
\frame{
  \frametitle{Community structure}
  %%
  \begin{itemize}
  \item Required outcome: a procedure to group nodes into clusters
    such that each resulting cluster is a ``meaningful'' community.

  \item Two basic approaches: partition the nodes or the links.  Two
    types of community: overlapping and non-overlapping.

  \item \textbf{Node partition}: a popular method is to optimize
    Newman's quality function $Q$~\cite{Newman2003a,NewmanGirvan2004},
    defined as
    \[
    Q
    =
    \frac{1}{2m}
    \sum_{ij} \left( A_{ij} - \frac{k_i k_j}{2m} \right)
    \delta(c_i, c_j)
    \]
    where the sum is over all nodes in the network.  The quantity $m$ is
    the number of edges in the network, $A_{ij}$ is the weight of the edge
    from $i$ to $j$, and $k_i$ is the sum of weights of all edges attached
    to $i$.  The delta function $\delta(c_i, c_j) = 1$ if $i$ and $j$
    belong to the same community, and $\delta(c_i, c_j) = 0$ otherwise.
  \end{itemize}
}
%%
\frame{
  \frametitle{Community structure}
  %%
  \begin{itemize}
  \item \textbf{Link partition}: work with the links
    directly~\cite{AhnEtAl2010} or work with a ``transformed''
    network~\cite{EvansLambiotte2009}.  In both cases, we can apply
    techniques used for node partitioning, e.g.~dendrogram and
    hierarchical clustering.
    %% The transformed network is a line graph of the original network.
    %% The line graph is constructed as follows.  Each link in the
    %% original network is treated as a node in the line graph.  Two
    %% nodes in the line graph are connected by a link if, in the
    %% original network, the corresponding links have a node in
    %% common.
  \end{itemize}
  %%
  \begin{figure}[!htbp]
  \centering
  \includegraphics[scale=0.8]{image/Zachary-karate-club-community}
  \caption{Two communities in the Zachary karate club
    network~\cite{Zachary1977}.}
  \end{figure}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Motifs}
%%
\frame{
  \frametitle{Network motifs}
  %%
  \begin{itemize}
  \item A structural pattern~(with $\geq 3$ nodes) that occurs more
    frequently than expected~\cite{MiloEtAl2002}.  The basic idea is
    to choose a structural pattern, compute its density in an
    empirical network, compute its density in a random
    network~(usually Erd\H{o}s-R\'enyi), and compare our results.
  \end{itemize}
  %%
  \begin{figure}[!htbp]
  \centering
  \subfigure[All motifs on $3$ nodes.]{
    \includegraphics[scale=0.6]{image/motif-3nodes}
  }
  %%
  \qquad
  %%
  \subfigure[All motifs on $4$ nodes.]{
    \includegraphics[scale=0.6]{image/motif-4nodes}
  }
  \caption{If we treat a motif as a network, then there must be a path
    from one node to any other distinct node.}
  %% A network motif must be a (weakly) connected graph.  In the case
  %% of a digraph, we first obtain all the possible motifs on n nodes.
  %% Then for each motif, we assign all possible edge direction.  The
  %% results should be all possible motifs in a digraph with n nodes.
  \end{figure}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Networks over time}
%%
\frame{
  \frametitle{Networks over time}
  %%
  \begin{itemize}
  \item Key question: How do we model real-world networks that evolve
    over time?  This depends on what we want to focus on.

  \item Barab\'asi-Albert model: evolution of the ``rich get richer''
    effect~\cite{BarabasiAlbert1999}.

  \item Forest fire model: evolution of power-law degree distribution
    and shrinking diameter~\cite{LeskovecEtAl2007}.

  \item Community life-cycle: evolution of communities, from birth to
    death.

  \item Applications: evolution of social networks, spread of
    epidemics, evolution of technological networks~(e.g.~the internet,
    WWW, software systems), anomaly detection in and
    resilience/robustness of computer networks.
  \end{itemize}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliographystyle{bibliography.bst}
\bibliography{bibliography}

\end{document}
